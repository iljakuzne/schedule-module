CREATE TABLE ad_schedule (
                            id SERIAL PRIMARY KEY,
                            ad_id bigint,
                            inventory_count: int);

create table time_slot (
                        id serial PRIMARY KEY,
                        ad_schedule int references ad_schedule(id),
                        date_from timestamp NOT NULL,
                        date_to timestamp NOT NULL);

create table time_slot_book (
                                id serial PRIMARY KEY,
                                time_slot int references time_slot(id),
                                order_id int,
                                created_at timestamp NOT NULL  DEFAULT NOW());

ALTER TABLE ad_schedule ADD COLUMN active BOOLEAN DEFAULT TRUE

