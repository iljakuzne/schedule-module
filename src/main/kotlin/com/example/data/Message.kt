package com.example.data

data class Message(val id: Int = 1,
                   val message: String = "Hello koin")
