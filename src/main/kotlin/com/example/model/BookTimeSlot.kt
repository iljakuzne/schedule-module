package com.example.model

import java.time.LocalDateTime

data class BookTimeSlot (val fromDate: LocalDateTime,
                         val toDate: LocalDateTime,
                         val bookCount: Long)