package com.example.model

data class CreateBookTimeSlotModel(val orderId: Long,
                                   val timeSlotIds: List<Long>)
