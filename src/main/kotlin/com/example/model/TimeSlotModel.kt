package com.example.model

import java.time.LocalDateTime

data class TimeSlotModel(val adScheduleId: Long,
                         val dateFrom: LocalDateTime,
                         val dateTo: LocalDateTime)
