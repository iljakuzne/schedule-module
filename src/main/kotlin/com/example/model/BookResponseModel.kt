package com.example.model

import java.util.*

data class BookResponseModel(val createdBook: List<Long>?,
                             val newSlots : List<TimeSlotResponse>?,
                             val statusMessage: String)
