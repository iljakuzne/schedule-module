package com.example.model

data class AdModel(val adId: Long,
                   val inventoryCount: Long)
