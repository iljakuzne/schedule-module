package com.example.model

import java.time.LocalDateTime

data class BookTimeSlotModel (val adId: Long,
                              val orderId: Long,
                              val inventoryCount: Int,
                              val dates: List<LocalDateTime>) {
}