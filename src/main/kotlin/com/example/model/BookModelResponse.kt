package com.example.model


import java.time.LocalDateTime

data class BookModelResponse (val orderId: Long,
                              val createdAt: LocalDateTime,
                              val slots: List<TimeSlotModel>)