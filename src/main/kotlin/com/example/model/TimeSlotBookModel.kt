package com.example.model

data class TimeSlotBookModel(val timeSlotId: Long,
                             val orderId: Long)
