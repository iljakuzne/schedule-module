package com.example.model

import java.time.LocalDateTime

data class TimeSlotResponse (val freeCount: Int,
                             val fromDate: LocalDateTime,
                             val toDate: LocalDateTime)