package com.example.web


import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import org.koin.core.qualifier.named
import org.koin.dsl.module

val restModule = module{
    single<BaseRouterConfiguration>{
        AdScheduleController(get())
    }
    single<BaseRouterConfiguration>((named("timeController"))){
        TimeSlotController(get(),get())
    }
    single<BaseRouterConfiguration>((named("timeBookController"))){
        TimeSlotBookController(get(),get())
    }
    single {
        val router = Router.router(get())
        router.route().handler(BodyHandler.create())
        val configs: List<BaseRouterConfiguration> = getAll()
        configs.forEach {config->config.configure(router)}
        router
    }
    single(qualifier=named("server"), createdAtStart = true ) {
        val vertx: Vertx = get()
        val router: Router = get()
        val configuration: JsonObject = get()
        val server = vertx.createHttpServer()
        server.requestHandler(router).listen(configuration.getInteger("port",8080))
    }
}


