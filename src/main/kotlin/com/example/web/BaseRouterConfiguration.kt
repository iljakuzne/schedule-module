package com.example.web

import io.vertx.ext.web.Router

interface BaseRouterConfiguration {

    fun configure (router: Router)
}