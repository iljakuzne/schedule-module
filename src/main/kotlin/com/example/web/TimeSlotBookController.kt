package com.example.web

import com.example.service.TimeSlotBookService
import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.Future
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import java.time.LocalDate
import java.time.LocalDateTime

class TimeSlotBookController(private val timeSlotBookService: TimeSlotBookService,
                             private val objectMapper: ObjectMapper) : BaseRouterConfiguration {

    override fun configure(router: Router) {
        router.post(reserveTimeSlot)
            .respond(::reserveTimeSlot)

        router.get(getBookTimeSlot)
            .respond(::getBookTimeSlot)

        router.post(reverseReserve)
            .respond(::clearBook)

        router.post(cancelReverseSlots)
            .respond(::clearBookSlot)
    }

    private fun clearBookSlot (context: RoutingContext) : Future<JsonObject>{
        val orderId = context.pathParam("orderId").toLong()
        val requestObject = context.bodyAsJson
        return timeSlotBookService.clearBookSlot(orderId,requestObject)
            .map {_->
                JsonObject()
                    .put("message","success clear reserve")
            }
    }

    private fun clearBook (context: RoutingContext) : Future<JsonObject> {
        val orderId = context.pathParam("orderId").toLong()
        return timeSlotBookService.clearBook(orderId)
            .map{_->
                JsonObject()
                    .put("message","success clear reserve")
            }

    }


    private fun getBookTimeSlot (context: RoutingContext): Future<JsonObject> {
        val adId = context.pathParam("adId").toLong()
        val date = LocalDate.parse(context.pathParam("date"))
        return timeSlotBookService.findBookByDateAidId(adId,date)
            .map{responses->
                val history = responses.map{res->
                    val resultObject = JsonObject()
                        .put("orderId",res.orderId)
                        .put("createdAt",objectMapper.writeValueAsString(res.createdAt))
                    val array = JsonArray()
                    res.slots.forEach{slot->
                        array.add(JsonObject()
                            .put("adScheduleId",slot.adScheduleId)
                            .put("dateFrom",objectMapper.writeValueAsString(slot.dateFrom))
                            .put("dateTo",objectMapper.writeValueAsString(slot.dateTo))
                            .toString())
                    }
                    resultObject.put("slots",array)
                }
                JsonObject()
                    .put("history",history)
            }
    }

    private fun reserveTimeSlot (context: RoutingContext) : Future<JsonObject>{
        val request = context.bodyAsJson
        return timeSlotBookService.bookedSlots(request)
            .map{responseModel->
                val result = JsonObject()
                responseModel.createdBook?.also {books->
                    val array = JsonArray()
                    books.forEach{l->array.add(l)}
                    result.put("createBook",array)
                }?:result.put("createBook",null)
                responseModel.newSlots?.also {slots->
                    val array = JsonArray()
                    slots.forEach {timeSlot->
                        val responseObject = JsonObject()
                            .put("dateFrom",objectMapper.writeValueAsString(timeSlot.fromDate))
                            .put("dateTo",objectMapper.writeValueAsString(timeSlot.toDate))
                            .put("freeCount",timeSlot.freeCount)
                            .toString()
                        array.add(responseObject)
                    }
                    result.put("newSlots",array)
                }?:result.put("newSlots",null)
                result.put("statusMessage",responseModel.statusMessage)
                result
            }
    }

    companion object {
        private const val baseUrl = "/api"
        const val reverseReserve = "${baseUrl}/reverse/:orderId"
        const val cancelReverseSlots = "${baseUrl}/reverseSlot/:orderId"
        const val reserveTimeSlot = "${baseUrl}/reserveSlots"
        const val getBookTimeSlot = "${baseUrl}/book/reserved/:adId/:date"
    }
}