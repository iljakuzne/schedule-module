package com.example.web

import com.example.service.TimeSlotService
import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.Future
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import java.time.LocalDate

class TimeSlotController (private val timeSlotService: TimeSlotService,
                          private val objectMapper: ObjectMapper) : BaseRouterConfiguration {

    override fun configure(router: Router) {
        router.get(getTimeSlots)
            .respond(::getSlotsByDate)

        router.get(getBookTimeSlot)
            .respond(::getBookSlot)
    }

    private fun getBookSlot (context: RoutingContext) : Future<JsonObject> {
        val adId = context.pathParam("adId").toLong()
        val date = LocalDate.parse(context.pathParam("date"))
        return timeSlotService.findBookSlots(adId,date)
            .map{bookTimeSlot->
                val array = JsonArray()
                bookTimeSlot.forEach {slot->
                    array.add(JsonObject()
                        .put("dateFrom",objectMapper.writeValueAsString(slot.fromDate))
                        .put("dateTo",objectMapper.writeValueAsString(slot.toDate))
                        .put("bookCount",slot.bookCount)
                        .toString())
                }
                JsonObject()
                    .put("slots",array)
            }
    }


    private fun getSlotsByDate (context: RoutingContext) : Future<JsonObject> {
        val adId = context.pathParam("adId").toLong()
        val date = LocalDate.parse(context.pathParam("date"))
        return timeSlotService.findFreeSlots(adId,date)
            .map{slotResponse->
                val array = JsonArray()
                slotResponse.forEach {slot->
                    val responseObject = JsonObject()
                        .put("dateFrom",objectMapper.writeValueAsString(slot.fromDate))
                        .put("dateTo",objectMapper.writeValueAsString(slot.toDate))
                        .put("freeCount",slot.freeCount)
                        .toString()
                    array.add(responseObject)
                }
                JsonObject().put("slots",array)
            }
            .onFailure {error->
                error.printStackTrace()
            }
    }

    companion object {
        private const val baseUrl = "/api"
        const val getTimeSlots  = "${baseUrl}/slots/:adId/:date" //выгрузка слотов
        const val getBookTimeSlot = "${baseUrl}/getBookSlots/:adId/:date" //выгрузка занятых слотов
    }
}