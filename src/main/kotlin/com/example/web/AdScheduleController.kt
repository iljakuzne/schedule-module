package com.example.web

import com.example.service.AdScheduleService
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext

class AdScheduleController (private val adScheduleService: AdScheduleService) : BaseRouterConfiguration {

    override fun configure(router: Router) {
        router.post(createAdScheduleUrl)
            .respond(::createAd)

        router.post(inactiveAd)
            .respond(::inactiveAd)
    }

    private fun createAd (ctx: RoutingContext) : Future<JsonObject> {
        return adScheduleService.createAdSchedule(ctx.getBodyAsJson())
            .map {id->
                JsonObject().put("id",id)
            }
    }

    private fun inactiveAd (context: RoutingContext) : Future<JsonObject>{
        val adId = context.pathParam("adId").toLong()
        return adScheduleService.inactiveAdSchedule(adId)
            .map{adScheduleId->
                JsonObject()
                    .put("scheduleId",adScheduleId)
            }
    }


    companion object {
        private const val baseUrl = "/api"
        const val inactiveAd = "${baseUrl}/adSchedule/inactive/:adId"
        const val createAdScheduleUrl = "${baseUrl}/adSchedule/create"
    }
}