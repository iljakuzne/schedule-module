package com.example.dao

import com.example.model.AdModel
import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.Tuple

class AdScheduleRepository : AbstractRepository() {

    fun createAdSchedule (connection: SqlConnection,
                          model: AdModel) : Future<Long> {
        val data = Tuple.of(model.adId,model.inventoryCount)
        return executeSqlSingleResult(connection,
            createAdSchedule,
            data){row->row.get(Long::class.javaObjectType,"id")}
    }

    fun existsByOrder (connection: SqlConnection,
                       orderId: Long) : Future<Boolean> {
        val data = Tuple.of(orderId.toInt())
        return executeSqlSingleResult(connection,
            existsByOrder,
            data){row->row.get(Boolean::class.javaObjectType,0)}
    }

   fun updateInactive (connection: SqlConnection,
                       adId: Long) : Future<Long> {
        val data = Tuple.of(false,adId)
        return executeSqlSingleResult(connection,
            updateInactive,
            data){}
        return connection.preparedQuery(updateActiveAd)
            .execute())
            .map{rows->
                rows.first().get(Long::class.javaObjectType,0)
            }
   }

    fun findForGenerateSlot (connection: SqlConnection, dayCreate: Long): Future<Any> {
        return connection.preparedQuery(findForGenerateSlots)
            .execute(Tuple.of())
    }

    companion object {

        const val findForGenerateSlots = """
            SELECT asc.id 
            FROM ad_schedule asc
            WHERE active = $1
            AND NOT EXISTS (SELECT id FROM time_slot where ad_schedule = asc.id AND date_from > $2)
        """

        const val updateInactive = "UPDATE ad_schedule SET active = $1 WHERE ad_id = :$2 RETURNING id"

        const val existsByOrder ="SELECT EXISTS (SELECT id FROM ad_schedule WHERE ad_id = $1)"

        const val createAdSchedule = "INSERT INTO ad_schedule (ad_id, inventory_count) VALUES ($1, $2) RETURNING id"
    }
}