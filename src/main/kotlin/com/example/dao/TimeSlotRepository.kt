package com.example.dao

import com.example.mappers.CreateBookTimeMapper
import com.example.model.BookTimeSlot
import com.example.model.TimeSlotModel
import com.example.model.TimeSlotResponse
import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.Tuple
import java.time.LocalDate
import java.time.LocalDateTime

class TimeSlotRepository(private val createBookTimeMapper: CreateBookTimeMapper) {

    fun getByDateRange (connection: SqlConnection,
                        adId: Long,
                        fromDate: LocalDate,
                        toDate: LocalDate) : Future<List<TimeSlotResponse>>{
        return connection.preparedQuery(getFreeTimeSlots)
            .execute(Tuple.of(adId,
                fromDate.atStartOfDay(),
                toDate.atStartOfDay()))
            .map {rowSet->
                rowSet.map(createBookTimeMapper::map)
            }
    }

    fun getFreeTimeSlotForBook (connection: SqlConnection,
                                adId: Long,
                                fromDates: List<LocalDateTime>) : Future<Map<LocalDateTime,List<Long>>> {
        return connection.preparedQuery(getSlotForBook)
            .execute(Tuple.of(adId)
                .addArrayOfLocalDateTime(fromDates.toTypedArray()))
            .map {rowSet->
                rowSet.groupByTo(
                    mutableMapOf(),
                    {row->row.get(LocalDateTime::class.java,"date_from")},
                    {row->row.get(Long::class.javaObjectType,"id")}
                )
            }
    }

   fun getBookSlot (connection: SqlConnection,
                     adId: Long,
                     fromDate: LocalDate,
                     toDate: LocalDate) : Future<List<BookTimeSlot>>{
        return connection.preparedQuery(getBookSlots)
            .execute(Tuple.of(adId,fromDate.atStartOfDay(),toDate.atStartOfDay()))
            .map {rows->
                rows.map(createBookTimeMapper::mapBookSlot)
            }
    }

    fun createTimeSlots (connection: SqlConnection,
                         models: List<TimeSlotModel>) : Future<List<Long>>{
        return connection
            .preparedQuery(insertQuery)
            .executeBatch(models.map {timeSlot->
                Tuple.of(timeSlot.adScheduleId,
                    timeSlot.dateFrom,
                    timeSlot.dateTo)
            })
            .map {res->
                res.map {row->
                    row.get(Long::class.javaObjectType,0)
                }
            }
    }

    companion object {

        const val getBookSlots = """
            SELECT 
            ts.date_from,
            ts.date_to,
            count (ts.id) as book_count
            FROM time_slot ts  
            JOIN ad_schedule sch on sch.id = ts.ad_schedule
            JOIN time_slot_book tsb on tsb.time_slot = ts.id
            WHERE sch.ad_id = $1
            AND ts.date_from BETWEEN $2 AND $3
            GROUP BY ts.date_from, ts.date_to
        """

        const val getSlotForBook = """
            SELECT
            ts.id,
            ts.date_from
            FROM time_slot ts 
            join ad_schedule sch on sch.id = ts.ad_schedule
            WHERE sch.ad_id = $1
            AND ts.date_from = ANY ($2)
            AND NOT EXISTS (SELECT id from time_slot_book where time_slot = ts.id)
        """

        const val getFreeTimeSlots = """
            SELECT 
            ts.date_from,
            ts.date_to,
            count(ts.id) as free_count
            FROM time_slot ts
            JOIN ad_schedule sch on ts.ad_schedule = sch.id
            WHERE sch.ad_id = $1
            AND ts.date_from BETWEEN $2 AND $3
            AND NOT EXISTS (SELECT id from time_slot_book where  time_slot = ts.id)
            GROUP BY ts.date_from, ts.date_to 
        """


        const val insertQuery = """
            INSERT INTO time_slot (ad_schedule,date_from,date_to)
            VALUES ($1, $2, $3)
            RETURNING id
        """
    }
}