package com.example.dao

import io.vertx.core.Future
import io.vertx.sqlclient.Row
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.Tuple

abstract class AbstractRepository {

    fun <T> executeSqlBatch (connection: SqlConnection,
                             query: String,
                             data: List<Tuple>,
                             mapper: (Row)->T) : Future<List<T>> {
        return connection.preparedQuery(query)
            .executeBatch(data)
            .map{rows->
                rows.map(mapper)
            }
    }


    fun <T> executeSqlSeveralResult (connection: SqlConnection,
                        query: String,
                        data: Tuple,
                        mapper: (Row)->T) : Future<List<T>>{
        return connection.preparedQuery(query)
            .execute(data)
            .map{rows->
                rows.map(mapper)
            }
    }


    fun <T> executeSqlSingleResult (connection: SqlConnection,
                        query: String,
                        data: Tuple,
                        mapper: (Row) -> T) : Future<T> {
        return connection.preparedQuery(query)
            .execute(data)
            .map{rows->
                rows.first().let(mapper)
            }
    }
}