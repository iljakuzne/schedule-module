package com.example.dao

import com.example.mappers.CreateBookTimeMapper
import com.example.model.BookModelResponse
import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.Tuple
import java.time.LocalDate
import java.time.LocalDateTime

class TimeSlotBookRepository(private val createBookTimeMapper: CreateBookTimeMapper) {

    fun createTimeSlotsBooks (connection: SqlConnection,
                              orderId: Long,
                              slotIds: List<Long>) : Future<List<Long>>{
        return connection.preparedQuery(createSql)
            .executeBatch(slotIds.map{slotId->
                Tuple.of(slotId,orderId)
            })
            .map{rows->
                rows.map{row ->
                    row.get(Long::class.javaObjectType,0)
                }
            }
    }

    fun findBookByDateRange (connection: SqlConnection,
                             adId: Long,
                             dateFrom:LocalDate,
                             dateTo: LocalDate) : Future<List<BookModelResponse>> {
        return connection.preparedQuery(bookedByDateRange)
            .execute(Tuple.of(adId,dateFrom.atStartOfDay(),dateTo.atStartOfDay()))
            .map{rows->
                rows.groupBy{row->row.get(Long::class.javaObjectType,"order_id")}
                    .map {entry->
                        BookModelResponse(orderId = entry.key,
                            createdAt = entry.value.first().get(LocalDateTime::class.java,"created_at"),
                            slots = entry.value.map(createBookTimeMapper::mapSlotModel))
                    }
            }
    }

    fun findByOrderId (connection: SqlConnection,
                       orderId: Long,
                       slots: List<LocalDateTime>) : Future<Map<LocalDateTime,List<Long>>>{
        return connection.preparedQuery(selectSlotIdQuery)
            .execute(Tuple.of(orderId).addArrayOfLocalDateTime(slots.toTypedArray()))
            .map {rows->
                rows.groupByTo(mutableMapOf(),
                    {row->row.getLocalDateTime("date_from")},
                    {row->row.getLong("id")})
            }
    }

    fun deleteBook (connection: SqlConnection,
                    orderId: Long) : Future<Boolean>{
        return connection.preparedQuery(deleteBook)
            .execute(Tuple.of(orderId))
            .map {_->
                true
            }
    }

    fun deleteBookSlot (connection: SqlConnection,
                        orderId: Long,
                        slotIds: List<Long>) : Future<Boolean> {
        return connection.preparedQuery(deleteByOrderIdAndSlots)
            .execute(Tuple.of(orderId).addArrayOfLong(slotIds.toTypedArray()))
            .map {_->
                true
            }
    }

    companion object {

        const val deleteByOrderIdAndSlots  = """
            DELETE FROM time_slot_book 
            WHERE order_id = $1
            AND time_slot = ANY ($2)
        """

        const val selectSlotIdQuery = """
            SELECT ts.id,
            ts.date_from
            FROM time_slot_book tsb
            JOIN time_slot ts on ts.id = tsb.time_slot 
            where order_id = $1
            AND ts.date_from = ANY ($2)
        """

        const val deleteBook = """
            DELETE FROM time_slot_book where order_id = $1
        """

        const val bookedByDateRange = """
            SELECT 
            ts.date_from,
            ts.date_to,
            ts.ad_schedule,
            tsb.order_id,
            tsb.created_at
            FROM time_slot_book tsb
            JOIN time_slot ts ON ts.id = tsb.time_slot
            WHERE EXISTS (select id from ad_schedule where id = ts.ad_schedule AND ad_id = $1)  
            AND ts.date_from BETWEEN $2 AND $3
        """

        const val createSql = "INSERT INTO time_slot_book (time_slot, order_id) " +
                "VALUES ($1, $2) RETURNING id"
    }
}