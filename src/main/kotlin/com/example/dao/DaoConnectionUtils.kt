package com.example.dao

import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.get

object DaoConnectionUtils {

    fun <T> transactionTemplate (handler: (SqlConnection)->Future<T>) : Future<T> {
        return get<Future<SqlConnection>>(Future::class.java, named("sqlConnection"))
            .compose {conn->
                conn.begin()
                    .compose {tx->
                        handler(conn)
                            .onFailure {error->
                                error.printStackTrace()
                                tx.rollback()
                            }
                            .onSuccess {_->tx.commit() }
                    }
                    .onComplete {conn.close()}
            }
    }

    fun <T>  connectionTemplate (handler: (SqlConnection)->Future<T>) : Future<T>{
        return get<Future<SqlConnection>>(Future::class.java, named("sqlConnection"))
            .compose {conn->
                handler(conn)
                    .onComplete {_->conn.close()}
            }
    }
}