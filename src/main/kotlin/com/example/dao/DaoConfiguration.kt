package com.example.dao

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.pgclient.PgConnectOptions
import io.vertx.pgclient.PgPool
import io.vertx.sqlclient.PoolOptions
import org.koin.core.qualifier.named
import org.koin.dsl.module

val daoModule = module{
    single(createdAtStart = true){
        val vertx: Vertx = get()
        val conConfiguration: JsonObject = get<JsonObject>().getJsonObject("database")
        val poolConfiguration: JsonObject = get<JsonObject>().getJsonObject("databasePool")
        val connectorOptions = PgConnectOptions()
            .setPort(conConfiguration.getInteger("port"))
            .setHost(conConfiguration.getString("host"))
            .setDatabase(conConfiguration.getString("name"))
            .setUser(conConfiguration.getString("login"))
            .setPassword(conConfiguration.getString("password"))
        val poolOptions = PoolOptions()
            .setMaxSize(poolConfiguration.getInteger("maxSize"))
        PgPool.pool(vertx,connectorOptions,poolOptions)
    }
    factory(named("sqlConnection")) {
        val pool: PgPool = get()
        pool.connection.onFailure {exception->
            exception.printStackTrace()
        }
    }
    single {AdScheduleRepository()}
    single{TimeSlotRepository(get())}
    single {TimeSlotBookRepository(get())}
}