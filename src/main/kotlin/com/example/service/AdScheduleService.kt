package com.example.service

import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.sqlclient.SqlConnection

interface AdScheduleService {

    fun createAdSchedule (request: JsonObject) : Future<Long>

    fun inactiveAdSchedule (adId: Long) : Future<Long>

    fun findSlotsModuleFromGeneration (connection: SqlConnection, daysCreate: Long) : Future<List<Long>>
}