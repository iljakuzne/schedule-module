package com.example.service

import com.example.dao.TimeSlotRepository
import com.example.model.BookTimeSlot
import com.example.model.TimeSlotModel
import com.example.model.TimeSlotResponse
import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.get
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit


class TimeSlotServiceImpl(private val timeSlotRepository: TimeSlotRepository) : TimeSlotService {


    override fun generateTimeSlot(adScheduleId: Long,
                                  countInventory: Int,
                                  period: Int,
                                  startTime: LocalDateTime,
                                  generatingDay: Long): Future<List<Long>> {
        return Future.succeededFuture(generateSlotList(adScheduleId,countInventory,period,startTime,generatingDay))
            .compose {slotsForSave->
            get<Future<SqlConnection>>(Future::class.java, named("sqlConnection"))
                .compose {conn->
                    conn.begin()
                        .compose {tx->
                            timeSlotRepository.createTimeSlots(conn,slotsForSave)
                                .onSuccess {
                                    tx.commit()
                                }
                                .onFailure {throwable->
                                    throwable.printStackTrace()
                                    tx.rollback()
                                }
                        }
                        .onComplete{_->
                            conn.close()
                        }
                }
        }
    }

    override fun findFreeSlots(adId: Long,
                               startDate: LocalDate): Future<List<TimeSlotResponse>> {
        return get<Future<SqlConnection>>(Future::class.java, named("sqlConnection"))
            .compose {conn->
                timeSlotRepository.getByDateRange(conn,adId,startDate,startDate.plusDays(1L))
                    .onFailure {error->
                        error.printStackTrace()
                    }
                    .onComplete {_->conn.close()}
            }
    }

    override fun findBookSlots(adId: Long,
                               startDate: LocalDate): Future<List<BookTimeSlot>> {
        return get<Future<SqlConnection>>(Future::class.java, named("sqlConnection"))
            .compose{conn->
                timeSlotRepository.getBookSlot(conn,adId,startDate,startDate.plusDays(1L))
                    .onFailure {error->error.printStackTrace()}
                    .onComplete {_->conn.close()}
            }
    }

    override fun findSlotsForBook(conn: SqlConnection,
                                  adId: Long,
                                  dates: List<LocalDateTime>): Future<Map<LocalDateTime,List<Long>>> {
        return timeSlotRepository.getFreeTimeSlotForBook(conn,adId,dates)
    }

    private fun generateSlotList (adScheduleId: Long,
                                  countInventory: Int,
                                  period: Int,
                                  startTime: LocalDateTime,
                                  generatingDay: Long) : List<TimeSlotModel> {
        val date = startTime.truncatedTo(ChronoUnit.MINUTES)
            .let{d->
                val minutes = d.minute
                if (minutes !in arrayOf(0,30)){
                    if (minutes > 30){
                        d.plusMinutes(60L-minutes)
                    } else {
                        d.plusMinutes(30L-minutes)
                    }
                }else {
                    d
                }
            }
        val endDate =  date.plus(generatingDay, ChronoUnit.DAYS)
        var bufferDate = date
        val slots = mutableListOf<TimeSlotModel>()
        while (bufferDate < endDate){
            val dateTo = bufferDate.plus(period.toLong(),ChronoUnit.MINUTES)
            repeat(countInventory){
                val slot = TimeSlotModel(adScheduleId = adScheduleId,
                    dateFrom = bufferDate,
                    dateTo = dateTo)
                slots.add(slot)
            }
            bufferDate = dateTo
        }
        return slots
    }
}