package com.example.service

import com.example.model.BookModelResponse
import com.example.model.BookResponseModel
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import java.time.LocalDate
import java.util.*

interface TimeSlotBookService {

    fun bookedSlots (request: JsonObject) : Future<BookResponseModel>

    fun findBookByDateAidId (aidId: Long,
                             date: LocalDate) : Future<List<BookModelResponse>>

    fun clearBook (orderId: Long) : Future<Boolean>

    fun clearBookSlot (orderId: Long,
                       request: JsonObject) : Future<Boolean>
}