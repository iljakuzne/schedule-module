package com.example.service

import com.example.dao.AdScheduleRepository
import com.example.dao.DaoConnectionUtils
import com.example.mappers.AdMapper
import io.vertx.core.Future
import io.vertx.core.impl.logging.LoggerFactory
import io.vertx.core.json.JsonObject
import io.vertx.sqlclient.SqlConnection
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.get
import java.lang.RuntimeException
import java.time.Instant
import java.time.LocalDateTime

class AdScheduleServiceImpl(private val adScheduleRepository: AdScheduleRepository,
                            private val timeSlotService: TimeSlotService) : AdScheduleService {

    private val logger = LoggerFactory.getLogger(AdScheduleServiceImpl::class.java)

    override fun createAdSchedule(request: JsonObject): Future<Long> {
        return DaoConnectionUtils.transactionTemplate {conn->
            val adModel = get<AdMapper>(AdMapper::class.java).mapCreate(request)
            adScheduleRepository.existsByOrder(conn,adModel.adId)
                .compose {exists->
                    if (exists){
                        throw RuntimeException("ad already exists")
                    } else {
                        adScheduleRepository.createAdSchedule(conn,adModel)
                    }
                }
                .onSuccess {adId->
                    timeSlotService.generateTimeSlot(adScheduleId = adId,
                        countInventory = adModel.inventoryCount.toInt(),
                        period = 30,
                        startTime = LocalDateTime.now(),
                        generatingDay = 5)
                        .onComplete {
                            logger.info("Слоты для объявления сгенерированы")
                        }}
        }
    }

    override fun inactiveAdSchedule(adId: Long): Future<Long> {
        return DaoConnectionUtils.transactionTemplate {conn->
            adScheduleRepository.updateInactive(conn,adId)
        }
    }

    override fun findSlotsModuleFromGeneration(connection: SqlConnection,
                                               daysCreate: Long): Future<List<Long>> {
        return
    }

}