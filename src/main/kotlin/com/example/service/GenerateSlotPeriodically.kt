package com.example.service

interface GenerateSlotPeriodically {

    fun generateSlotPeriodically (id: Long)
}