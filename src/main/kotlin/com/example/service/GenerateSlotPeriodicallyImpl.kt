package com.example.service

import io.vertx.core.Vertx
import io.vertx.core.impl.logging.LoggerFactory

class GenerateSlotPeriodicallyImpl(private val vertx: Vertx) : GenerateSlotPeriodically {

    private val logger = LoggerFactory.getLogger(GenerateSlotPeriodicallyImpl::class.java)

    init {
        vertx.setPeriodic(86400000, ::generateSlotPeriodically)
    }


    override fun generateSlotPeriodically(id: Long) {

    }
}