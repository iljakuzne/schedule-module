package com.example.service

import org.koin.dsl.module

val serviceModule = module {
    single<AdScheduleService>{
        AdScheduleServiceImpl(get(),get())
    }
    single<TimeSlotService>{
        TimeSlotServiceImpl(get())
    }
    single<TimeSlotBookService>{
        TimeSlotBookServiceImpl(get(),get(),get())
    }
    single<GenerateSlotPeriodically>(createdAtStart = true){
        GenerateSlotPeriodicallyImpl(get())
    }
}