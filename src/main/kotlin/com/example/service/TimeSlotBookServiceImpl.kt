package com.example.service

import com.example.dao.TimeSlotBookRepository
import com.example.mappers.CreateBookTimeMapper
import com.example.model.BookModelResponse
import com.example.model.BookResponseModel
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.sqlclient.SqlConnection
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.get
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class TimeSlotBookServiceImpl(private val createBookTimeMapper: CreateBookTimeMapper,
                              private val timeSlotService: TimeSlotService,
                              private val timeSlotBookRepository: TimeSlotBookRepository) : TimeSlotBookService {

    override fun bookedSlots(request: JsonObject): Future<BookResponseModel> {
        return get<Future<SqlConnection>>(Future::class.java,named("sqlConnection"))
            .compose{conn->
                conn.begin()
                    .compose {tx->
                        val createBookModel = createBookTimeMapper.mapBookSlot(request)
                        timeSlotService.findSlotsForBook(conn,createBookModel.adId,createBookModel.dates)
                            .compose {groupSlots->
                                if (groupSlots.size != createBookModel.dates.size){
                                    timeSlotService.findFreeSlots(createBookModel.adId,
                                        createBookModel.dates.first().toLocalDate())
                                        .map{timeSlots->
                                            BookResponseModel(createdBook = null,
                                                newSlots = timeSlots,
                                                statusMessage = "На некоторых забронированных слотах не хватает количества, обновите расписание")
                                        }
                                } else {
                                    val notHaveInventoryCount = groupSlots.values.filter {list->list.size < createBookModel.inventoryCount}
                                    if (notHaveInventoryCount.isNotEmpty()){
                                        timeSlotService.findFreeSlots(createBookModel.adId,
                                            createBookModel.dates.first().toLocalDate())
                                            .map{timeSlots->
                                                BookResponseModel(createdBook = null,
                                                    newSlots = timeSlots,
                                                    statusMessage = "На некоторых забронированных слотах не хватает количества, обновите расписание")
                                            }
                                    } else {
                                        val slotIds = groupSlots.values.flatMap {list->
                                            list.subList(0,createBookModel.inventoryCount)
                                        }
                                        timeSlotBookRepository.createTimeSlotsBooks(conn,createBookModel.orderId,slotIds)
                                            .map {createdBook->
                                                BookResponseModel(createdBook = createdBook,
                                                    newSlots = null,
                                                    statusMessage = "Слоты созданы")
                                            }
                                    }
                                }
                            }
                            .onSuccess {_->tx.commit()}
                            .onFailure {error->tx.rollback()
                                error.printStackTrace()
                            }
                    }
                    .onComplete {conn.close()}
            }
    }

    override fun findBookByDateAidId(aidId: Long,
                                     date: LocalDate): Future<List<BookModelResponse>> {
        return get<Future<SqlConnection>>(Future::class.java,named("sqlConnection"))
            .compose {conn->
                timeSlotBookRepository.findBookByDateRange(conn,aidId,date,date.plusDays(1L))
                    .onFailure {error->error.printStackTrace()}
                    .onComplete {_->conn.close()}
            }
    }

    override fun clearBook(orderId: Long): Future<Boolean> {
       return get<Future<SqlConnection>>(Future::class.java,named("sqlConnection"))
            .compose {conn->
                conn.begin()
                    .compose {tx->
                        timeSlotBookRepository.deleteBook(conn,orderId)
                            .onSuccess {_->tx.commit() }
                            .onFailure {error->tx.rollback()
                                error.printStackTrace()}
                    }
                    .onSuccess {_->conn.close()}
            }
    }

    override fun clearBookSlot(orderId: Long, request: JsonObject): Future<Boolean> {
        val inventoryCount = request.getLong("inventoryCount")
        val jsonArray = request.getJsonArray("slots")
        val slotDates = mutableListOf<LocalDateTime>()
        for (i in 0 until jsonArray.size()){
            slotDates.add(LocalDateTime.parse(jsonArray.getString(i)))
        }
        return get<Future<SqlConnection>>(Future::class.java,named("sqlConnection"))
            .compose {conn->
                conn.begin()
                    .compose{ tx->
                        timeSlotBookRepository.findByOrderId(conn,orderId,slotDates)
                            .compose {map->
                                val deletedLockSlots = mutableListOf<Long>()
                                map.forEach {entry->
                                    if (entry.value.size <=inventoryCount){
                                        deletedLockSlots.addAll(entry.value)
                                    } else {
                                        deletedLockSlots.addAll(entry.value.subList(0,inventoryCount.toInt()))
                                    }
                                }
                                timeSlotBookRepository.deleteBookSlot(conn,orderId,deletedLockSlots)
                            }
                            .onFailure {error->
                                error.printStackTrace()
                                tx.rollback()
                            }
                            .onSuccess {_-> tx.commit() }
                    }
                    .onComplete {_->conn.close() }
            }
    }
}