package com.example.service

import com.example.model.BookTimeSlot
import com.example.model.TimeSlotResponse
import io.vertx.core.Future
import io.vertx.sqlclient.SqlConnection
import java.time.LocalDate
import java.time.LocalDateTime

interface TimeSlotService {

    fun generateTimeSlot (adScheduleId: Long,
                          countInventory: Int,
                          period: Int,
                          startTime: LocalDateTime,
                          generatingDay: Long) : Future<List<Long>>

    fun findFreeSlots (adId: Long,
                       startDate: LocalDate) : Future<List<TimeSlotResponse>>

    fun findBookSlots (adId: Long,
                       startDate: LocalDate) : Future<List<BookTimeSlot>>

    fun findSlotsForBook (conn: SqlConnection,
                          adId: Long,
                          dates: List<LocalDateTime>) : Future<Map<LocalDateTime,List<Long>>>
}