package com.example.verticle

import com.example.dao.daoModule
import com.example.mappers.mapperModule
import com.example.service.serviceModule
import com.example.utils.utilsModule
import com.example.web.restModule
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.http.HttpServer
import io.vertx.sqlclient.SqlConnection
import org.koin.core.context.startKoin
import org.koin.core.logger.PrintLogger
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent.get


class MainVerticle : AbstractVerticle() {

    override fun start() {
        val vertxModule = module{
            single{vertx}
            single{config()}
        }
        startKoin{
            logger(PrintLogger())
            modules(vertxModule,
                daoModule,
                mapperModule,
                serviceModule,
                restModule,
                utilsModule)
        }
    }
}