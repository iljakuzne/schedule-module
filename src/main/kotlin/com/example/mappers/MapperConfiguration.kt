package com.example.mappers

import org.koin.dsl.module

val mapperModule = module{
    single {AdMapper()}
    single {CreateBookTimeMapper(get())}
    single {MapperDaoUtils()}
}