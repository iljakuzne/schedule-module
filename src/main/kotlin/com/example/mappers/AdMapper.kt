package com.example.mappers

import com.example.model.AdModel
import io.vertx.core.json.JsonObject

class AdMapper {

    fun mapCreate (obj: JsonObject) : AdModel {
        return AdModel(adId = obj.getLong("adId"),
            inventoryCount = obj.getLong("inventoryCount"))
    }
}