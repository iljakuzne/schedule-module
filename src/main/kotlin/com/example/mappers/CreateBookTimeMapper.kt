package com.example.mappers

import com.example.model.*
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.vertx.core.json.JsonObject
import io.vertx.sqlclient.Row
import java.awt.print.Book
import java.time.LocalDateTime

class CreateBookTimeMapper(private val objectMapper: ObjectMapper) {

    fun map (request: JsonObject) : CreateBookTimeSlotModel {
        val slotsIds = mutableListOf<Long>()
        val array = request.getJsonArray("slots")
        for (i in 0 until array.size()){
            slotsIds.add(array.getLong(i))
        }
        return CreateBookTimeSlotModel(orderId = request.getLong("orderId"),
        timeSlotIds = slotsIds)
    }

    fun mapBookSlot (row: Row) : BookTimeSlot{
        return BookTimeSlot(fromDate = row.get(LocalDateTime::class.java,"date_from"),
            toDate = row.get(LocalDateTime::class.java,"date_to"),
            bookCount = row.get(Long::class.javaObjectType,"book_count"))
    }

    fun map (row: Row) : TimeSlotResponse {
        return TimeSlotResponse(fromDate = row.get(LocalDateTime::class.java,"date_from"),
            toDate = row.get(LocalDateTime::class.java,"date_to"),
            freeCount = row.get(Int::class.javaObjectType,"free_count"))
    }

    fun mapBookSlot (request: JsonObject) : BookTimeSlotModel{
        val slotDated = mutableListOf<LocalDateTime>()
        val array = request.getJsonArray("dates")
        for (i in 0 until array.size()){
            slotDated.add(LocalDateTime.parse(array.getString(i)))
        }
        return BookTimeSlotModel(inventoryCount = request.getInteger("inventoryCount"),
            dates = slotDated,
            adId = request.getLong("adId"),
            orderId = request.getLong("orderId"))
    }

    fun mapSlotModel (request: Row) : TimeSlotModel{
        return TimeSlotModel(adScheduleId = request.get(Long::class.javaObjectType,"ad_schedule"),
            dateTo = request.getLocalDateTime("date_from"),
            dateFrom = request.getLocalDateTime("date_to"))
    }
}