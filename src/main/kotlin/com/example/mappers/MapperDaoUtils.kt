package com.example.mappers

import io.vertx.sqlclient.Row

class MapperDaoUtils {

    fun <T> convertToTypeIndex(row: Row, clz: Class<T>, index: Int) : T {
        return row.get(clz,index)
    }

  //  fun <T> convertToType  convertSingleResultType (row: Row, clz: Class<T>) : T {
//
  //  }

    fun convertRowLong (row: Row) : Long{
        return row.get(Long::class.javaObjectType,0)
    }
}