package com.example.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JSR310Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.koin.dsl.module
import java.text.SimpleDateFormat

val utilsModule = module {

    single{ObjectMapper()
        .registerModule(JavaTimeModule())
        .registerModule(KotlinModule())
        .setDateFormat(SimpleDateFormat("dd-MM-yyyy hh:mm"))
    }
}